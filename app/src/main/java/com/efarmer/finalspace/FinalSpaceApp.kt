package com.batynchuk.finalspace

import android.app.Application
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@HiltAndroidApp
class FinalSpaceApp : Application() {

}

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    @Provides
    fun provideWorkerDispatcher(): CoroutineDispatcher {
        return Dispatchers.IO
    }
}