package com.batynchuk.finalspace.common.base

import android.widget.Toast
import androidx.fragment.app.Fragment

open class BaseFragment : Fragment() {

    protected fun showError(error: String?) {
        error?.let {
            Toast.makeText(
                requireContext(),
                error,
                Toast.LENGTH_SHORT
            ).show()
        }
    }

}