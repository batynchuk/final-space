package com.batynchuk.finalspace.common.util

sealed class Result<out T> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
    data class Loading<out T : Any>(val cachedData: T) : Result<T>()
}