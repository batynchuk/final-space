package com.batynchuk.finalspace.common.base

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.batynchuk.finalspace.common.FinalSpaceRouter

abstract class BaseActivity : AppCompatActivity(), FinalSpaceRouter {

    abstract val fragmentContainerId: Int

    override fun show(fragment: Fragment, backStackTag: String?, isAnimated: Boolean) {
        buildTransaction(fragment, backStackTag, isAnimated).commit()
    }

    override fun goBack() {
        supportFragmentManager.popBackStack()
    }

    open fun buildTransaction(
        fragment: Fragment,
        backStackTag: String?,
        isAnimated: Boolean
    ): FragmentTransaction {
        val transaction = supportFragmentManager.beginTransaction()

        if (isAnimated) {
            transaction.setCustomAnimations(0, 0)
        }

        transaction.replace(fragmentContainerId, fragment, fragment.javaClass.simpleName)

        if (backStackTag != null) {
            transaction.addToBackStack(backStackTag)
        }

        return transaction
    }


}