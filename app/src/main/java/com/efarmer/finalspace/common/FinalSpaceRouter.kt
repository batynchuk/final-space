package com.batynchuk.finalspace.common

import androidx.fragment.app.Fragment

interface FinalSpaceRouter {

    fun show(fragment: Fragment, backStackTag: String? = null, isAnimated: Boolean = false)

    fun goBack()

}
