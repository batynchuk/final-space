package com.batynchuk.finalspace.common.base

import android.app.Activity
import com.batynchuk.finalspace.common.FinalSpaceRouter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@InstallIn(ActivityComponent::class)
@Module
object BaseActivityModule {
    @Provides
    fun provideBaseActivity(activity: Activity): BaseActivity {
        return activity as BaseActivity
    }
}

@Module
@InstallIn(ActivityComponent::class)
object FinalSpaceRouterModule {

    @Provides
    fun bindFinalSpaceRouter(
        baseActivity: BaseActivity
    ): FinalSpaceRouter {
        return baseActivity
    }
}