package com.batynchuk.finalspace.screens.main

import android.os.Bundle
import com.batynchuk.finalspace.R
import com.batynchuk.finalspace.common.base.BaseActivity
import com.batynchuk.finalspace.screens.characterlist.CharacterListFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    override val fragmentContainerId: Int
        get() = R.id.fl_container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            show(CharacterListFragment())
        }
    }
}