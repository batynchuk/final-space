package com.batynchuk.finalspace.screens.characterdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.batynchuk.finalspace.R
import com.batynchuk.finalspace.common.base.BaseFragment
import com.batynchuk.finalspace.common.util.Result
import com.batynchuk.finalspace.data.entity.Character
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.character_details_fragment.*

@AndroidEntryPoint
class CharacterDetailsFragment : BaseFragment() {

    companion object {
        private const val CHARACTER_ID_KEY = "CHARACTER_ID_KEY"
        fun newInstance(characterId: Int): CharacterDetailsFragment =
            CharacterDetailsFragment().also { fragment ->
                fragment.arguments = Bundle().apply { putInt(CHARACTER_ID_KEY, characterId) }
            }
    }

    private val viewModel by viewModels<CharacterDetailsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.character_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
    }

    private fun observeData() {
        viewModel.getCharacterById(arguments?.getInt(CHARACTER_ID_KEY) ?: 0)
            .observe(viewLifecycleOwner, { result ->
                pb_loading.isVisible = result is Result.Loading
                when (result) {
                    is Result.Success -> showCharacterData(result.data)
                    is Result.Error -> showError(result.exception.message)
                    is Result.Loading -> showCharacterData(result.cachedData)

                }
            })
    }

    private fun showCharacterData(character: Character?) {
        character?.let {
            Glide.with(iv_photo)
                .load(character.img_url)
                .into(iv_photo)
            tv_name.text = character.name
            tv_origin.text = character.origin
            tv_species.text = character.species
            tv_alias.text = getString(R.string.also_known, character.alias.joinToString())
            tv_abilities.text =
                getString(R.string.abilities, character.abilities.joinToString())
        }
    }
}