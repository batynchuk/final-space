package com.batynchuk.finalspace.screens.characterlist

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.batynchuk.finalspace.data.repositories.CharacterRepository

class CharacterListViewModel @ViewModelInject constructor(
    private val repository: CharacterRepository
) : ViewModel() {

    val characterLoaderData = repository.getCharacterList()

}
