package com.batynchuk.finalspace.screens.characterdetails

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.batynchuk.finalspace.data.repositories.CharacterRepository

class CharacterDetailsViewModel @ViewModelInject constructor(
    private val repository: CharacterRepository
) : ViewModel() {

    fun getCharacterById(id: Int) = repository.getCharacterById(id)

}