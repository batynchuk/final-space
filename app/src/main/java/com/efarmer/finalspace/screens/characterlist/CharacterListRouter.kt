package com.batynchuk.finalspace.screens.characterlist

import com.batynchuk.finalspace.common.FinalSpaceRouter
import com.batynchuk.finalspace.common.base.FinalSpaceRouterModule
import com.batynchuk.finalspace.data.entity.Character
import com.batynchuk.finalspace.screens.characterdetails.CharacterDetailsFragment
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import javax.inject.Inject

interface CharacterListRouter {

    fun showCharacterDetailsScreen(character: Character)

}

class CharacterListNavigationRouter @Inject constructor(private val finalSpaceRouter: FinalSpaceRouter) :
    CharacterListRouter {
    override fun showCharacterDetailsScreen(character: Character) {
        finalSpaceRouter.show(
            CharacterDetailsFragment.newInstance(character.id),
            CharacterDetailsFragment::class.java.name,
            true
        )
    }
}

@Module(includes = [FinalSpaceRouterModule::class])
@InstallIn(FragmentComponent::class)
abstract class CharacterListModule {

    @Binds
    abstract fun bindCharacterListRouter(router: CharacterListNavigationRouter): CharacterListRouter

}