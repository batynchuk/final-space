package com.batynchuk.finalspace.screens.characterlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.batynchuk.finalspace.R
import com.batynchuk.finalspace.common.base.BaseFragment
import com.batynchuk.finalspace.common.util.Result
import com.batynchuk.finalspace.screens.characterlist.adapter.CharactersController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.character_list_fragment.*
import javax.inject.Inject

@AndroidEntryPoint
class CharacterListFragment : BaseFragment() {

    @Inject
    lateinit var router: CharacterListRouter

    private lateinit var charactersController: CharactersController

    private val viewModel: CharacterListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.character_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        charactersController = CharactersController { router.showCharacterDetailsScreen(it) }
        observeData()
        rv_characters.layoutManager = LinearLayoutManager(requireContext())
        rv_characters.adapter = charactersController.adapter
    }

    private fun observeData() {
        viewModel.characterLoaderData.observe(viewLifecycleOwner, { charactersResult ->
            pb_loading.isVisible = charactersResult is Result.Loading
            when (charactersResult) {
                is Result.Success -> charactersController.setData(charactersResult.data)
                is Result.Error -> showError(charactersResult.exception.message)
                is Result.Loading -> charactersController.setData(charactersResult.cachedData)
            }
        })
    }

}