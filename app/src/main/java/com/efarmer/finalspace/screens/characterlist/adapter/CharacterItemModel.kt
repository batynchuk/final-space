package com.batynchuk.finalspace.screens.characterlist.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.batynchuk.finalspace.R
import kotlinx.android.synthetic.main.item_character.view.*

@EpoxyModelClass(layout = R.layout.item_character)
abstract class CharacterItemModel : EpoxyModelWithHolder<CharacterHolder>() {

    @EpoxyAttribute
    var name: String = ""

    @EpoxyAttribute
    var imageUrl: String? = null

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var onItemClick: ((View) -> Unit)? = null

    override fun bind(holder: CharacterHolder) {
        super.bind(holder)
        holder.rootView.setOnClickListener(onItemClick)
        holder.tvName.text = name
        imageUrl?.let { url ->
            Glide.with(holder.ivPhoto)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivPhoto)
        }
    }
}

class CharacterHolder : EpoxyHolder() {

    lateinit var rootView: View
    lateinit var tvName: TextView
    lateinit var ivPhoto: ImageView

    override fun bindView(itemView: View) {
        rootView = itemView
        tvName = itemView.tv_name
        ivPhoto = itemView.iv_photo
    }

}