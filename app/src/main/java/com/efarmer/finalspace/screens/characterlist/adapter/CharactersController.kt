package com.batynchuk.finalspace.screens.characterlist.adapter

import com.airbnb.epoxy.TypedEpoxyController
import com.batynchuk.finalspace.data.entity.Character

class CharactersController(private val onCharacterClick: (Character) -> Unit) :
    TypedEpoxyController<List<Character>?>() {

    override fun buildModels(characters: List<Character>?) {

        characters?.forEach { character ->
            CharacterItemModel_()
                .id(character.id)
                .name(character.name)
                .imageUrl(character.img_url)
                .onItemClick { onCharacterClick(character) }
                .addTo(this)
        }

    }

}