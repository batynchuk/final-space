package com.batynchuk.finalspace.data.db

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ApplicationComponent::class)
object DbModule {

    @Provides
    fun provideFinalSpaceDb(@ApplicationContext context: Context): FinalSpaceDb {
        return Room.databaseBuilder(context, FinalSpaceDb::class.java, DB_NAME).build()
    }

    @Provides
    fun provideCharacterDao(db: FinalSpaceDb): CharactersDao {
        return db.characterDao()
    }
}