package com.batynchuk.finalspace.data.network

import com.batynchuk.finalspace.data.entity.Character
import retrofit2.http.GET
import retrofit2.http.Path

interface FinalSpaceService {

    @GET("character/")
    suspend fun getAllCharacters(): List<Character>

    @GET("character/{id}")
    suspend fun getCharacterDetails(@Path("id") id: Int): Character

}