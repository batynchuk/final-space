package com.batynchuk.finalspace.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.batynchuk.finalspace.data.entity.Character

const val DB_NAME = "finalSpace"

@Database(entities = [Character::class], version = 1, exportSchema = false)
abstract class FinalSpaceDb : RoomDatabase() {

    abstract fun characterDao(): CharactersDao

}