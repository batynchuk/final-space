package com.batynchuk.finalspace.data.db

import androidx.room.TypeConverter

class StringListConverter {
    @TypeConverter
    fun fromList(hobbies: List<String?>): String {
        return hobbies.joinToString(",")
    }

    @TypeConverter
    fun toList(data: String): List<String> {
        return data.split(',')
    }
}