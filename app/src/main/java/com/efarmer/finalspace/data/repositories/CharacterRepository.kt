package com.batynchuk.finalspace.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import com.batynchuk.finalspace.data.db.CharactersDao
import com.batynchuk.finalspace.data.entity.Character
import com.batynchuk.finalspace.data.network.FinalSpaceService
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject
import com.batynchuk.finalspace.common.util.Result

interface CharacterRepository {

    fun getCharacterById(characterId: Int): LiveData<Result<Character>>

    fun getCharacterList(): LiveData<Result<List<Character>>>

}

// Probably better solution would be to build an abstract remote and local DataSources,
// But for project of this size it's not crucial
class CharacterRepositoryImpl @Inject constructor(
    private val characterDao: CharactersDao,
    private val finalSpaceService: FinalSpaceService,
    private val workerDispatcher: CoroutineDispatcher
) :
    CharacterRepository {
    override fun getCharacterById(characterId: Int): LiveData<Result<Character>> {
        return liveData {
            val characterFlow = characterDao.getById(characterId)
                .distinctUntilChanged()
                .flowOn(workerDispatcher)
            val disposable = emitSource(
                characterFlow.map { Result.Loading(it) }.asLiveData()
            )
            try {
                withContext(workerDispatcher) {
                    val character = finalSpaceService.getCharacterDetails(characterId)
                    disposable.dispose()
                    characterDao.updateCharacter(character)
                    emitSource(characterFlow.map { Result.Success(it) }.asLiveData())
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                emit(Result.Error(ex))
                emitSource(characterFlow.map { Result.Success(it) }.asLiveData())
            }

        }
    }

    override fun getCharacterList(): LiveData<Result<List<Character>>> {
        return liveData {
            val charactersFlow = characterDao.getAll()
                .distinctUntilChanged()
                .flowOn(workerDispatcher)
            val disposable = emitSource(
                charactersFlow
                    .map { Result.Loading(it) }
                    .asLiveData()
            )
            try {
                withContext(workerDispatcher) {
                    val list = finalSpaceService.getAllCharacters()
                    disposable.dispose()
                    characterDao.updateData(list)
                    emitSource(charactersFlow.map { Result.Success(it) }.asLiveData())

                }
            } catch (ex: Exception) { // we can handle HttpException here, and get error code if one is documented by the api
                ex.printStackTrace()
                emit(Result.Error(ex))
                emitSource(charactersFlow.map { Result.Success(it) }.asLiveData())
            }
        }
    }

}

@Module
@InstallIn(ApplicationComponent::class)
abstract class CharacterRepositoryModule {

    @Binds
    abstract fun bindCharacterRepository(repository: CharacterRepositoryImpl): CharacterRepository

}
