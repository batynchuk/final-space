package com.batynchuk.finalspace.data.db

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.batynchuk.finalspace.data.entity.Character
import kotlinx.coroutines.flow.Flow

@Dao
interface CharactersDao : BaseDao<Character> {

    @Query("SELECT * FROM character WHERE id = :id")
    fun getById(id: Int): Flow<Character>

    @Query("SELECT * FROM character")
    fun getAll(): Flow<List<Character>>

    @Query("DELETE FROM character")
    fun deleteAll()

    @Transaction
    fun updateData(list: List<Character>) {
        deleteAll()
        insertAll(list)
    }

    @Update
    fun updateCharacter(character: Character)

}