package com.batynchuk.finalspace.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface BaseDao<T> {

    @Insert(onConflict = REPLACE)
    fun insert(item: T)

    @Insert(onConflict = REPLACE)
    fun insertAll(list: List<T>)

    @Delete
    fun delete(item: T)

}