package com.batynchuk.finalspace.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.batynchuk.finalspace.data.db.StringListConverter

@Entity
@TypeConverters(StringListConverter::class)
data class Character(
    val abilities: List<String?>,
    val alias: List<String?>,
    val gender: String?,
    val hair: String?,
    @PrimaryKey
    val id: Int,
    val img_url: String,
    val name: String,
    val origin: String?,
    val species: String?,
    val status: String?
)